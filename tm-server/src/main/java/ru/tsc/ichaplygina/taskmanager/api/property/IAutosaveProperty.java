package ru.tsc.ichaplygina.taskmanager.api.property;

import org.jetbrains.annotations.NotNull;

public interface IAutosaveProperty {

    @NotNull Integer getAutosaveFrequency();

    @NotNull Boolean getAutosaveOnExit();

}
