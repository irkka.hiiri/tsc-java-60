package ru.tsc.ichaplygina.taskmanager.service.dto;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.ichaplygina.taskmanager.api.repository.dto.IProjectRecordRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.dto.IProjectRecordService;
import ru.tsc.ichaplygina.taskmanager.dto.ProjectDTO;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.tsc.ichaplygina.taskmanager.util.ComparatorUtil.getComparator;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

@Service
@AllArgsConstructor
public final class ProjectRecordService extends AbstractBusinessEntityRecordService<ProjectDTO> implements IProjectRecordService {

    @NotNull
    private IProjectRecordRepository repository;

    @Override
    @SneakyThrows
    @Transactional
    public void add(@NotNull final ProjectDTO project) {
        repository.add(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@NotNull final String userId, @NotNull final String name, @Nullable final String description) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO(name, description, userId);
        add(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<ProjectDTO> projectList) {
        if (projectList == null) return;
        for (final ProjectDTO project : projectList) add(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        repository.clear();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(final String userId) {
        if (userService.isPrivilegedUser(userId)) repository.clear();
        else repository.clearForUser(userId);
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO completeById(@NotNull final String userId, @Nullable final String projectId) {
        return updateStatus(userId, projectId, Status.COMPLETED);
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO completeByIndex(@NotNull final String userId, final int index) {
        if (isInvalidListIndex(index, repository.getSize())) throw new IndexIncorrectException(index);
        @Nullable final String id = userService.isPrivilegedUser(userId) ?
                repository.getIdByIndex(index) :
                repository.getIdByIndexForUser(userId, index);
        return completeById(userId, id);
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO completeByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @Nullable final String id = userService.isPrivilegedUser(userId) ?
                repository.getIdByName(name) :
                repository.getIdByNameForUser(userId, name);
        if (isEmptyString(id)) return null;
        return completeById(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        return userService.isPrivilegedUser(userId) ?
                repository.findAll() : repository.findAllForUser(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(@NotNull final String userId, @Nullable final String sortBy) {
        @NotNull final Comparator<ProjectDTO> comparator = getComparator(sortBy);
        return userService.isPrivilegedUser(userId) ?
                repository.findAll().stream().sorted(comparator).collect(Collectors.toList()) :
                repository.findAllForUser(userId).stream().sorted(comparator).collect(Collectors.toList());
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        return repository.findById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findById(@NotNull final String userId, @Nullable final String projectId) {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        return userService.isPrivilegedUser(userId) ?
                repository.findById(projectId) : repository.findByIdForUser(userId, projectId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findByIndex(@NotNull final String userId, final int entityIndex) {
        if (isInvalidListIndex(entityIndex, getSize())) throw new IndexIncorrectException(entityIndex + 1);
        return userService.isPrivilegedUser(userId) ?
                repository.findByIndex(entityIndex) : repository.findByIndexForUser(userId, entityIndex);
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findByName(@NotNull final String userId, @NotNull final String entityName) {
        if (isEmptyString(entityName)) throw new NameEmptyException();
        return userService.isPrivilegedUser(userId) ?
                repository.findByName(entityName) : repository.findByNameForUser(userId, entityName);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, final int index) {
        return userService.isPrivilegedUser(userId) ?
                repository.getIdByIndex(index) : repository.getIdByIndexForUser(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, @NotNull final String entityName) {
        return userService.isPrivilegedUser(userId) ?
                repository.getIdByName(entityName) : repository.getIdByNameForUser(userId, entityName);
    }

    @Override
    @SneakyThrows
    public long getSize() {
        return repository.getSize();
    }

    @Override
    @SneakyThrows
    public long getSize(@NotNull final String userId) {
        return userService.isPrivilegedUser(userId) ?
                repository.getSize() : repository.getSizeForUser(userId);
    }

    @Override
    @SneakyThrows
    public boolean isEmpty() {
        return getSize() == 0;
    }

    @Override
    @SneakyThrows
    public boolean isEmpty(@NotNull final String userId) {
        return getSize(userId) == 0;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO removeById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @Nullable final ProjectDTO project = findById(id);
        if (project == null) return null;
        repository.removeById(id);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO removeById(@NotNull final String userId, @NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @Nullable final ProjectDTO project = findById(userId, id);
        if (project == null) return null;
        if (userService.isPrivilegedUser(userId)) repository.removeById(id);
        else repository.removeByIdForUser(userId, id);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO removeByIndex(@NotNull final String userId, final int index) {
        if (isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        @Nullable final ProjectDTO project = findByIndex(userId, index);
        if (project == null) return null;
        if (userService.isPrivilegedUser(userId)) repository.removeByIndex(index);
        else repository.removeByIndexForUser(userId, index);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO removeByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @Nullable final ProjectDTO project = findByName(userId, name);
        if (project == null) return null;
        if (userService.isPrivilegedUser(userId)) repository.removeByName(name);
        else repository.removeByNameForUser(userId, name);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO startById(@NotNull final String userId, @Nullable final String projectId) {
        return updateStatus(userId, projectId, Status.IN_PROGRESS);
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO startByIndex(@NotNull final String userId, final int index) {
        if (isInvalidListIndex(index, repository.getSize())) throw new IndexIncorrectException(index);
        @Nullable final String id = userService.isPrivilegedUser(userId) ?
                repository.getIdByIndex(index) :
                repository.getIdByIndexForUser(userId, index);
        return startById(userId, id);
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO startByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @Nullable final String id = userService.isPrivilegedUser(userId) ?
                repository.getIdByName(name) :
                repository.getIdByNameForUser(userId, name);
        return startById(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO updateById(@NotNull final String userId,
                                 @NotNull final String projectId,
                                 @NotNull final String projectName,
                                 @Nullable final String projectDescription) {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        if (isEmptyString(projectName)) throw new NameEmptyException();
        @Nullable final ProjectDTO project = findById(userId, projectId);
        if (project == null) return null;
        project.setName(projectName);
        project.setDescription(projectDescription);
        repository.update(project);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO updateByIndex(@NotNull final String userId,
                                    final int entityIndex,
                                    @NotNull final String entityName,
                                    @Nullable final String entityDescription) {
        if (isInvalidListIndex(entityIndex, getSize(userId))) throw new IndexIncorrectException(entityIndex + 1);
        @Nullable final String entityId = Optional.ofNullable(getId(userId, entityIndex)).orElse(null);
        if (entityId == null) return null;
        return updateById(userId, entityId, entityName, entityDescription);
    }

    @Nullable
    @Transactional
    private ProjectDTO updateStatus(@NotNull final String userId, @Nullable final String projectId, @NotNull final Status status) {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        @Nullable final ProjectDTO project = findById(userId, projectId);
        if (project == null) return null;
        project.setStatus(status);
        repository.update(project);
        return project;
    }

}
