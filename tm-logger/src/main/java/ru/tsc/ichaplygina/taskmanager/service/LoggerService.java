package ru.tsc.ichaplygina.taskmanager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.ichaplygina.taskmanager.api.service.ILoggerService;
import ru.tsc.ichaplygina.taskmanager.dto.EntityLogDTO;

import java.io.File;
import java.io.FileOutputStream;

import static ru.tsc.ichaplygina.taskmanager.constant.FileNameConst.*;

@Service
public class LoggerService implements ILoggerService {

    @Override
    @SneakyThrows
    public void writeLog(@NotNull final EntityLogDTO message) {
        @Nullable final String className = message.getClassName();
        @Nullable final String fileName = getFileName(className);
        if (fileName == null) return;
        @NotNull final File file = new File(fileName);
        file.getParentFile().mkdirs();
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file, true);
        @NotNull final String header = "Id: " + message.getId() + "; Type: " + message.getType() + "; Date: " + message.getDate() + "\n";
        fileOutputStream.write(header.getBytes());
        fileOutputStream.write(message.getEntity().getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    private String getFileName(@NotNull final String className) {
        switch (className) {
            case "ProjectDTO":
            case "Project":
                return PROJECT_LOG_FILE_NAME;
            case "TaskDTO":
            case "Task":
                return TASK_LOG_FILE_NAME;
            case "UserDTO":
            case "User":
                return USER_LOG_FILE_NAME;
            case "SessionDTO":
            case "Session":
                return SESSION_LOG_FILE_NAME;
            default:
                return null;
        }
    }

}
