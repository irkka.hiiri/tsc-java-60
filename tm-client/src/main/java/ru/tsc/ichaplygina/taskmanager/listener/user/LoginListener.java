package ru.tsc.ichaplygina.taskmanager.listener.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.endpoint.Session;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class LoginListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "login into the system";
    @NotNull
    public static final String NAME = "login";

    @NotNull
    @Override
    public final String command() {
        return NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@loginListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final String login = readLine(ENTER_LOGIN);
        @NotNull final String password = readLine(ENTER_PASSWORD);
        @Nullable final Session currentSession = sessionService.getSession();
        if (currentSession != null) getSessionEndpoint().closeSession(currentSession);
        final Session session = getSessionEndpoint().openSession(login, password);
        sessionService.setSession(session);
    }

}
