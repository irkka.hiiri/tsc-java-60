package ru.tsc.ichaplygina.taskmanager.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.ichaplygina.taskmanager.api.repository.model.IUserRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.IUserService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.empty.EmailEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.LoginEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.PasswordEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserExistsWithEmailException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserExistsWithLoginException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IncorrectCredentialsException;
import ru.tsc.ichaplygina.taskmanager.model.User;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.HashUtil.salt;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Autowired
    private IUserRepository repository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    @SneakyThrows
    @Transactional
    public final void add(@NotNull final String login, @NotNull final String password, @NotNull final String email, @NotNull final Role role,
                          @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        if (isEmptyString(email)) throw new EmailEmptyException();
        @NotNull final User user = new User(login, Objects.requireNonNull(salt(password, propertyService)), email, firstName, middleName, lastName, role);
        add(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public final void add(@NotNull final User user) {
        if (!isEmptyString(repository.findIdByLogin(user.getLogin())))
            throw new UserExistsWithLoginException(user.getLogin());
        if (!isEmptyString(repository.findIdByEmail(user.getEmail())))
            throw new UserExistsWithEmailException(user.getEmail());
        repository.add(user);
    }

    @Override
    @Transactional
    public void addAll(@Nullable List<User> users) {
        if (users == null) return;
        for (final User user : users) add(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        repository.clear();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<User> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        return repository.findById(id);
    }

    @Nullable
    @Override
    public final User findByLogin(@NotNull final String login) {
        return Optional.ofNullable(findUserByLogin(login)).orElseThrow(UserNotFoundException::new);
    }

    @Nullable
    @Override
    public final User findByLoginForAuthorization(@NotNull final String login) {
        return Optional.ofNullable(findUserByLogin(login)).orElseThrow(IncorrectCredentialsException::new);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String findIdByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        return repository.findIdByLogin(login);

    }

    @Nullable
    private User findUserByLogin(@NotNull final String login) {
        return repository.findByLogin(login);
    }

    @Override
    @SneakyThrows
    public long getSize() {
        return repository.getSize();
    }

    @Override
    @SneakyThrows
    public boolean isEmpty() {
        return (getSize() == 0);
    }

    @Override
    public final boolean isPrivilegedUser(@NotNull final String userId) {
        @NotNull final User user = Optional.ofNullable(findById(userId)).orElseThrow(UserNotFoundException::new);
        return user.getRole().equals(Role.ADMIN);
    }

    @Override
    @SneakyThrows
    @Transactional
    public final boolean lockById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final User user = Optional.ofNullable(repository.findById(id)).orElseThrow(UserNotFoundException::new);
        if (user.isLocked()) return false;
        user.setLocked(true);
        repository.update(user);
        return true;
    }

    @Override
    @SneakyThrows
    @Transactional
    public final boolean lockByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final User user = Optional.ofNullable(repository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        if (user.isLocked()) return false;
        user.setLocked(true);
        repository.update(user);
        return true;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User removeById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final User user = Optional.ofNullable(repository.findById(id)).orElseThrow(UserNotFoundException::new);
        repository.removeById(id);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public final User removeByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new IdEmptyException();
        @NotNull final User user = Optional.ofNullable(repository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        repository.removeByLogin(login);
        return user;
    }

    @Override
    @SneakyThrows
    @Transactional
    public final void setPassword(@NotNull final String login, @NotNull final String password) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        @NotNull final User user = Optional.ofNullable(findByLogin(login)).orElseThrow(UserNotFoundException::new);
        user.setPasswordHash(salt(password, propertyService));
        repository.update(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public final void setRole(@NotNull final String login, @NotNull final Role role) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final User user = Optional.ofNullable(findByLogin(login)).orElseThrow(UserNotFoundException::new);
        user.setRole(role);
        repository.update(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public final boolean unlockById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final User user = Optional.ofNullable(repository.findById(id)).orElseThrow(UserNotFoundException::new);
        if (!user.isLocked()) return false;
        user.setLocked(false);
        repository.update(user);
        return true;
    }

    @Override
    @SneakyThrows
    @Transactional
    public final boolean unlockByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final User user = Optional.ofNullable(repository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        if (!user.isLocked()) return false;
        user.setLocked(false);
        repository.update(user);
        return true;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public final User updateById(@NotNull final String id, @NotNull final String login, @NotNull final String password, @NotNull final String email,
                                 @NotNull final Role role, @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        if (isEmptyString(email)) throw new EmailEmptyException();
        @Nullable final User userFoundWithThisLogin = repository.findByLogin(login);
        @Nullable final User userFoundWithThisEmail = repository.findByEmail(email);
        if (userFoundWithThisLogin != null && !id.equals(userFoundWithThisLogin.getId()))
            throw new UserExistsWithLoginException(login);
        if (userFoundWithThisEmail != null && !id.equals(userFoundWithThisEmail.getId()))
            throw new UserExistsWithEmailException(email);
        @NotNull final User user = Optional.ofNullable(findById(id)).orElseThrow(UserNotFoundException::new);
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setEmail(email);
        user.setRole(role);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        repository.update(user);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public final User updateByLogin(@NotNull final String login, @NotNull final String password, @NotNull final String email,
                                    @NotNull final Role role, @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final String id = Optional.ofNullable(findIdByLogin(login)).orElseThrow(UserNotFoundException::new);
        return updateById(id, login, password, email, role, firstName, middleName, lastName);
    }

}
