package ru.tsc.ichaplygina.taskmanager.listener.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

@Component
public class DomainSaveJsonFasterXMLListener extends AbstractDomainListener {

    @NotNull
    public final static String DESCRIPTION = "save projects, tasks and users to json file using fasterxml";
    @NotNull
    public final static String NAME = "save json fasterxml";

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@domainSaveJsonFasterXMLListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        getAdminEndpoint().saveJsonFasterXML(sessionService.getSession());
    }

}
