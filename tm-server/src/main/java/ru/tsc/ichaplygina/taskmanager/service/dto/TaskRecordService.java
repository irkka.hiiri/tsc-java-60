package ru.tsc.ichaplygina.taskmanager.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.ichaplygina.taskmanager.api.repository.dto.ITaskRecordRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.dto.ITaskRecordService;
import ru.tsc.ichaplygina.taskmanager.api.service.dto.IUserRecordService;
import ru.tsc.ichaplygina.taskmanager.dto.TaskDTO;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.tsc.ichaplygina.taskmanager.util.ComparatorUtil.getComparator;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class TaskRecordService extends AbstractBusinessEntityRecordService<TaskDTO> implements ITaskRecordService {

    @NotNull
    @Autowired
    private ITaskRecordRepository repository;

    @NotNull
    @Autowired
    private IUserRecordService userService;

    @Override
    @SneakyThrows
    @Transactional
    public void add(@NotNull final TaskDTO task) {
        repository.add(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@NotNull final String userId, @NotNull final String name, @Nullable final String description) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final TaskDTO task = new TaskDTO(name, description, userId);
        add(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<TaskDTO> taskList) {
        if (taskList == null) return;
        for (final TaskDTO task : taskList) add(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public final TaskDTO addTaskToProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        @Nullable final TaskDTO task = userService.isPrivilegedUser(userId) ?
                repository.findById(taskId) : repository.findByIdForUser(userId, taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        repository.clear();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(final String userId) {
        if (userService.isPrivilegedUser(userId)) repository.clear();
        else repository.clearForUser(userId);
    }

    @Nullable
    @Override
    @Transactional
    public TaskDTO completeById(@NotNull final String userId, @Nullable final String taskId) {
        return updateStatus(userId, taskId, Status.COMPLETED);
    }

    @Nullable
    @Override
    @Transactional
    public TaskDTO completeByIndex(@NotNull final String userId, final int index) {
        if (isInvalidListIndex(index, repository.getSize())) throw new IndexIncorrectException(index);
        @Nullable final String id = userService.isPrivilegedUser(userId) ?
                repository.getIdByIndex(index) :
                repository.getIdByIndexForUser(userId, index);
        return completeById(userId, id);
    }

    @Nullable
    @Override
    @Transactional
    public TaskDTO completeByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @Nullable final String id = userService.isPrivilegedUser(userId) ?
                repository.getIdByName(name) :
                repository.getIdByNameForUser(userId, name);
        if (id == null) return null;
        return completeById(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(@NotNull final String userId) {
        return userService.isPrivilegedUser(userId) ?
                repository.findAll() : repository.findAllForUser(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(@NotNull final String userId, @Nullable final String sortBy) {
        @NotNull final Comparator<TaskDTO> comparator = getComparator(sortBy);
        return userService.isPrivilegedUser(userId) ?
                repository.findAll().stream().sorted(comparator).collect(Collectors.toList()) :
                repository.findAllForUser(userId).stream().sorted(comparator).collect(Collectors.toList());
    }

    @NotNull
    @Override
    @SneakyThrows
    public final List<TaskDTO> findAllByProjectId(@NotNull final String userId,
                                                  @NotNull final String projectId,
                                                  @Nullable final String sortBy) {
        @NotNull final Comparator<TaskDTO> comparator = getComparator(sortBy);
        return userService.isPrivilegedUser(userId) ?
                repository.findAllByProjectId(projectId).stream().sorted(comparator).collect(Collectors.toList()) :
                repository.findAllByProjectIdForUser(userId, projectId).stream().sorted(comparator).collect(Collectors.toList());
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        return repository.findById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findById(@NotNull final String userId, @Nullable final String taskId) {
        if (isEmptyString(taskId)) throw new IdEmptyException();
        return userService.isPrivilegedUser(userId) ?
                repository.findById(taskId) : repository.findByIdForUser(userId, taskId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findByIndex(@NotNull final String userId, final int entityIndex) {
        if (isInvalidListIndex(entityIndex, getSize())) throw new IndexIncorrectException(entityIndex + 1);
        return userService.isPrivilegedUser(userId) ?
                repository.findByIndex(entityIndex) : repository.findByIndexForUser(userId, entityIndex);

    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findByName(@NotNull final String userId, @NotNull final String entityName) {
        if (isEmptyString(entityName)) throw new NameEmptyException();
        return userService.isPrivilegedUser(userId) ?
                repository.findByName(entityName) : repository.findByNameForUser(userId, entityName);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, final int index) {
        return userService.isPrivilegedUser(userId) ?
                repository.getIdByIndex(index) : repository.getIdByIndexForUser(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, @NotNull final String entityName) {
        return userService.isPrivilegedUser(userId) ?
                repository.getIdByName(entityName) : repository.getIdByNameForUser(userId, entityName);
    }

    @Override
    @SneakyThrows
    public long getSize() {
        return repository.getSize();
    }

    @Override
    @SneakyThrows
    public long getSize(@NotNull final String userId) {
        return userService.isPrivilegedUser(userId) ?
                repository.getSize() : repository.getSizeForUser(userId);
    }

    @Override
    @SneakyThrows
    public boolean isEmpty() {
        return getSize() == 0;
    }

    @Override
    @SneakyThrows
    public boolean isEmpty(@NotNull final String userId) {
        return getSize(userId) == 0;
    }

    @Override
    @SneakyThrows
    @Transactional
    public final void removeAllByProjectId(@NotNull final String projectId) {
        repository.removeAllByProjectId(projectId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO removeById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @Nullable final TaskDTO task = findById(id);
        if (task == null) return null;
        repository.removeById(id);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO removeById(@NotNull final String userId, @NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @Nullable final TaskDTO task = findById(userId, id);
        if (task == null) return null;
        if (userService.isPrivilegedUser(userId)) repository.removeById(id);
        else repository.removeByIdForUser(userId, id);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO removeByIndex(@NotNull final String userId, final int index) {
        if (isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        @Nullable final TaskDTO task = findByIndex(userId, index);
        if (task == null) return null;
        if (userService.isPrivilegedUser(userId)) repository.removeByIndex(index);
        else repository.removeByIndexForUser(userId, index);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO removeByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @Nullable final TaskDTO task = findByName(userId, name);
        if (task == null) return null;
        if (userService.isPrivilegedUser(userId)) repository.removeByName(name);
        else repository.removeByNameForUser(userId, name);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public final TaskDTO removeTaskFromProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        @Nullable final TaskDTO task = userService.isPrivilegedUser(userId) ?
                repository.findTaskInProject(taskId, projectId) : repository.findTaskInProjectForUser(userId, taskId, projectId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        task.setProjectId(null);
        repository.update(task);
        return task;
    }

    @Nullable
    @Override
    @Transactional
    public TaskDTO startById(@NotNull final String userId, @Nullable final String taskId) {
        return updateStatus(userId, taskId, Status.IN_PROGRESS);
    }

    @Nullable
    @Override
    @Transactional
    public TaskDTO startByIndex(@NotNull final String userId, final int index) {
        if (isInvalidListIndex(index, repository.getSize())) throw new IndexIncorrectException(index);
        @Nullable final String id = userService.isPrivilegedUser(userId) ?
                repository.getIdByIndex(index) :
                repository.getIdByIndexForUser(userId, index);
        return startById(userId, id);
    }

    @Nullable
    @Override
    @Transactional
    public TaskDTO startByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @Nullable final String id = userService.isPrivilegedUser(userId) ?
                repository.getIdByName(name) :
                repository.getIdByNameForUser(userId, name);
        return startById(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO updateById(@NotNull final String userId,
                              @NotNull final String taskId,
                              @NotNull final String taskName,
                              @Nullable final String taskDescription) {
        if (isEmptyString(taskId)) throw new IdEmptyException();
        if (isEmptyString(taskName)) throw new NameEmptyException();
        @Nullable final TaskDTO task = findById(userId, taskId);
        if (task == null) return null;
        task.setName(taskName);
        task.setDescription(taskDescription);
        repository.update(task);
        return task;
    }

    @Nullable
    @Override
    @Transactional
    public TaskDTO updateByIndex(@NotNull final String userId,
                                 final int entityIndex,
                                 @NotNull final String entityName,
                                 @Nullable final String entityDescription) {
        if (isInvalidListIndex(entityIndex, getSize(userId))) throw new IndexIncorrectException(entityIndex + 1);
        @Nullable final String entityId = Optional.ofNullable(getId(userId, entityIndex)).orElse(null);
        if (entityId == null) return null;
        return updateById(userId, entityId, entityName, entityDescription);
    }

    @Nullable
    @Transactional
    private TaskDTO updateStatus(@NotNull final String userId, @Nullable final String taskId, @NotNull final Status status) {
        if (isEmptyString(taskId)) throw new IdEmptyException();
        @Nullable final TaskDTO task = findById(userId, taskId);
        if (task == null) return null;
        task.setStatus(status);
        repository.update(task);
        return task;
    }

}
