package ru.tsc.ichaplygina.taskmanager.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class UserUpdateByLoginListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "update user by login";
    @NotNull
    public static final String NAME = "update user by login";

    @NotNull
    @Override
    public final String command() {
        return NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userUpdateByLoginListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final String login = readLine(ENTER_LOGIN);
        @NotNull final String password = readLine(ENTER_PASSWORD);
        @NotNull final String email = readLine(ENTER_EMAIL);
        @NotNull final String role = readLine(ENTER_ROLE);
        @NotNull final String firstName = readLine(ENTER_FIRST_NAME);
        @NotNull final String middleName = readLine(ENTER_MIDDLE_NAME);
        @NotNull final String lastName = readLine(ENTER_LAST_NAME);
        getAdminEndpoint().updateUserByLogin(sessionService.getSession(), login, password, email, role, firstName, middleName, lastName);
    }

}
