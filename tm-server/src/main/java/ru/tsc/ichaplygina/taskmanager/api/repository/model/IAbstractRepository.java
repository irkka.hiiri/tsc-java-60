package ru.tsc.ichaplygina.taskmanager.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.List;

public interface IAbstractRepository<E extends AbstractModel> {

    void add(@NotNull E entity);

    void clear();

    @NotNull
    List<E> findAll();

    @Nullable
    E findById(@NotNull String id);

    long getSize();

    @NotNull
    EntityManager getEntityManager();

    void removeById(@NotNull String id);

    void update(E entity);
}
