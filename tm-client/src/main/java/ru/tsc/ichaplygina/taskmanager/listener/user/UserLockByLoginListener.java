package ru.tsc.ichaplygina.taskmanager.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class UserLockByLoginListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "lock user by login";
    @NotNull
    public static final String NAME = "lock user by login";

    @NotNull
    @Override
    public final String command() {
        return NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userLockByLoginListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final String login = readLine(ENTER_LOGIN);
        getAdminEndpoint().lockUserByLogin(sessionService.getSession(), login);
    }

}
